package eg.edu.alexu.csd.oop.db;

public interface Parser {
	public boolean regexChecker(String theRegex , String query , int queryLength);
	public Object parse(String query)throws java.sql.SQLException;
	public int[] selectAfterOperator(String attribute ,String condition, String[][] tableStr , String operator,String operandPart)throws java.sql.SQLException;
	public int[] selectBeforeOperator(String attribute, int colLenght,String part )throws java.sql.SQLException;
	
}