package eg.edu.alexu.csd.oop.sql;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import eg.edu.alexu.csd.oop.xml.XmlWriter;
public class StructureQueryParser extends MyParser{
	private String curDb;
	public StructureQueryParser(String db){
		curDb=db;
	}
	public String getDb(){
		return curDb;
	}
	private void deleteFile(File element) {
	    if (element.isDirectory()){
	        for (File sub : element.listFiles()) {
	            deleteFile(sub);
	        }
	    }
	    element.delete();
	}
	@Override
	public Object parse(String query) throws SQLException {
		String reg1 = "(\\s*CREATE\\s+DATABASE\\s+)(.*\\S)(\\s*)";
		String reg2 = "(\\s*DROP\\s+DATABASE\\s+)(.*\\S)(\\s*)";
		String reg3 = "(\\s*[Cc][Rr][Ee][Aa][Tt][Ee]\\s+[Tt][Aa][Bb][Ll][Ee]\\s+)(.*\\S)(\\s*)(\\(.+\\))(\\s*)";
		String reg4 = "(\\s*DROP\\s+TABLE\\s+)(.*\\S)(\\s*)";
		
		if (regexChecker(reg1,query,query.length())){
			return createDb(query.replaceAll(reg1,"$2").toLowerCase());
		}
		else if (regexChecker(reg2,query,query.length())){
			return dropDb(query.replaceAll(reg2,"$2").toLowerCase());
		}
		else if (regexChecker(reg3,query,query.length())){
			if (curDb==null)throw new SQLException(query+" "+"create table without database !!!!!!!!!!");
			String tableName = query.replaceAll(reg3,"$2");
			String col = query.replaceAll(reg3,"$4").replaceAll("^(\\()|(\\))$","").replaceAll("^(\\s*)|(\\s*)$","").
					replaceAll("\\s*,\\s*",",").replaceAll("\\s+[Ii][Nn][Tt]",";int").replaceAll("\\s+[Vv][Aa][Rr][Cc][Hh][Aa][Rr]",";varchar");
			return createTable(tableName.toLowerCase(),curDb+File.separator+tableName.toLowerCase()+".xml",col);
		}
		else if (regexChecker(reg4,query,query.length())){
			if (curDb==null)throw new SQLException(query+" "+"drop table without database !!!!!!!!!!");
			return dropTable(curDb+File.separator+query.replaceAll(reg4,"$2").toLowerCase()+".xml");
		}
		else { 
			throw new SQLException("SQL Syntax Error(Creation)");
		}
	}
	private boolean createDb (String db){	
		File f = new File(db);
		if(f.exists()&&f.isDirectory()){
			 curDb=db;
			 return true;
		}
		else{
			if(f.mkdir()){
				curDb = db;
				return true;
			}
			return false;
		}
	}
	private boolean dropDb(String db){
		File f = new File(db);
		if (f.exists() && f.isDirectory()){
	        for (File sub : new File(db).listFiles()) {
	            sub.delete();
	        }
			return true;
		}
		return false;
	}
	
	private boolean dropTable(String table_name){
		File f = new File(table_name);
		if (f.exists()&&!f.isDirectory()){
			deleteFile(f);
			return true;
		}
		return false;
	}
	private boolean createTable(String table_name,String path,String attr){
		File f = new File(path);
		if (f.exists()&&!f.isDirectory()){
			return false;
		}
		else {
			try {
				f.createNewFile();
			} catch (IOException e){
				return false;
			}
			if (f.exists()){
				new XmlWriter(f,new String[][]{},attr,table_name);
				return true;
			}
			return false;
		}
	}
}
