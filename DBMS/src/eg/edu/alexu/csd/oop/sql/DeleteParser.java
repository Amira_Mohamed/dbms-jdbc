package eg.edu.alexu.csd.oop.sql;
import java.io.File;
import java.sql.SQLException;
import eg.edu.alexu.csd.oop.xml.XmlReader;
import eg.edu.alexu.csd.oop.xml.XmlWriter;

public class DeleteParser extends MyParser{
	private String query,dataBaseName, fileName ,attribute;
	private XmlReader read;
	private int numberOfDelete = 0 ;
	private int[] deleteRow;
	private String [][]tableArray;
	
	public DeleteParser(String dataName) {
		this.dataBaseName = dataName;
	}
	
	@Override
	public Object parse(String query) throws SQLException {
		this.query = query;
		String check1 = "\\s*DELETE\\s+FROM\\s+(\\w+)\\s*";
		String check2 = "\\s*DELETE\\s+FROM\\s+(\\w+)\\s+WHERE\\s+(\\w+)\\s*([<,>,=])\\s*(.+)\\s*";
		
		if(this.regexChecker(check1, this.query, this.query.length()))
		{
			String []group = this.getGroups();
			String tableName = group[0];
			this.intialize(tableName);
			File file = new File(this.fileName);
			String[][] deletedAll = new String[0][0];
			this.numberOfDelete = this.tableArray.length;
			new XmlWriter( file, deletedAll ,this.attribute,tableName);	
		}
		//////////////////////////////////////////////////////////////////////////
		else if(this.regexChecker(check2, this.query, this.query.length()))
		{
			String []group = this.getGroups();
			String operator = group[2];
			String condition = group[1];
			String tableName = group[0];
			String operandValue = group[3];
			this.intialize(tableName);
			File file = new File(this.fileName);
		    this.deleteRow = this.selectAfterOperator(this.attribute, condition, this.tableArray, operator, operandValue);
			 String [][] updatedTable = null;	 
				if(this.getReturnedVal() != 0)
				{
					updatedTable = new String[this.tableArray.length-this.getReturnedVal()][this.tableArray[0].length];
					int index = 0 ;
					this.numberOfDelete = this.getReturnedVal();
					for(int i = 0 ; i < this.tableArray.length ; i++)
					{
						if(this.deleteRow[i] != 1){
							for(int j = 0 ; j <  this.tableArray[0].length ; j++)
							{
								updatedTable[index][j] = this.tableArray[i][j];
							}
							index++;
						}
					}
					new XmlWriter( file, updatedTable ,this.attribute, tableName);
				}	  
		 }
		else
			throw  new SQLException(this.query + "inside");
		
		return numberOfDelete;
	}
	private void intialize(String tableName) throws SQLException
	{
		this.fileName = this.dataBaseName+File.separatorChar+tableName.toLowerCase()+".xml";
		
	    try {
			this.read = new XmlReader(fileName);
		} catch (RuntimeException e) {
		} 
	   this.tableArray = this.read.getEntries();
	  this.attribute = this.read.getAtrr();
	}
}
