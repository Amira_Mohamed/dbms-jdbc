package eg.edu.alexu.csd.oop.sql;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import eg.edu.alexu.csd.oop.db.Parser;

public abstract class MyParser implements Parser{
	 private int returnedVal = 0,index = 0;
	 private  String query = null ;
	 private String[]groups;
	 
	@Override
	public abstract Object parse(String query) throws SQLException;
		
	@Override
		
	public boolean regexChecker(String theRegex, String query, int queryLength) {
			int matchNumber = 0, lengthOfMatcher = 0 ;
			this.query = query;
			 Pattern checkRegex = Pattern.compile(theRegex,Pattern.CASE_INSENSITIVE);
			 Matcher regexMatcher = checkRegex.matcher(this.query);
			 int groupLenght = regexMatcher.groupCount();
			 this.groups = new String[groupLenght];
			  while ( regexMatcher.find()){
				  lengthOfMatcher = regexMatcher.group().length();
				  if ( lengthOfMatcher != 0) 
					  matchNumber++;  
				  for(int i = 0 ; i < groupLenght ;i++)
						 groups[i] = regexMatcher.group(i+1);
			  }
			  if(matchNumber == 1)
			  {
				  if(lengthOfMatcher == queryLength)
					  return true;	  
			  }
			return false;
		}
		@Override
		public int[] selectAfterOperator(String attribute, String condition,String[][] tableArray,
			String operator,String operandVal) throws SQLException {
			int []selected = new int [1000000];
			String[]attributeVal = attribute.split(",");
			int indexCondition = -1;
			for(int i = 0 ; i < attributeVal.length ; i++)
			{
				String []attrName = attributeVal[i].split(";"); 
				if(attrName[0].equalsIgnoreCase(condition))
					indexCondition = i;	
			}
			if(indexCondition != -1)
			{		
				for(int m = 0 ; m < tableArray.length;m++)
				{
					if(operator.equals("="))
					{
						operandVal = operandVal.replaceAll("'", "");
						if(tableArray[m][indexCondition].equalsIgnoreCase(operandVal))
						 {	selected[m] = 1;
						    this.returnedVal++;
						 }
					}
					else if(operator.equals("<"))
					{
						int val1 = Integer.parseInt(tableArray[m][indexCondition]);
						int val2 = Integer.parseInt(operandVal);
						if(val1  < val2)
							{
							selected[m] = 1;
							this.returnedVal++;
							}
						
					}
					else if(operator.equals(">"))
					{
						int val1 = Integer.parseInt(tableArray[m][indexCondition]);
						int val2 = Integer.parseInt(operandVal);
						if(val1 > val2)
							{
							selected[m] = 1;
							this.returnedVal++;
							}    
					}
				}
	        }
			else
				throw  new SQLException(this.query + "after operator");
			return selected;	
			}

		@Override
		public int[] selectBeforeOperator(String attribute, int colLength, String part) throws SQLException {
			String []selectedcolumn = null;
			int [] selectedcolIndex = new int [10000];
			this.index = 0;
			if(part.contains("*"))
			{
				 for(int i = 0 ; i < colLength ; i++)
				    {
				    	selectedcolIndex[i] = i;
				    	this.index++;	
				    }
				    return Arrays.copyOfRange(selectedcolIndex, 0, this.index);
			}
			part = part.replaceAll("[\\s*]", "");
			if(part.contains(","))
			{
				selectedcolumn = part.split(",");
			}
			else
			{
				selectedcolumn = new String[1];
				selectedcolumn[0] = part;
			}
			this.index = 0;
			String[]attributeVal = attribute.split(",");
			for(int j = 0 ; j < selectedcolumn.length ; j++)
			{
				for(int i = 0 ; i < attributeVal.length ; i++)
				{
					String []attrName = attributeVal[i].split(";");
					if(attrName[0].equalsIgnoreCase(selectedcolumn[j]))
					{
						selectedcolIndex[index++] = i;
					}
					
				}
			}
			if(this.index != selectedcolumn.length)
				throw  new SQLException("before operator " + query );

			return Arrays.copyOfRange(selectedcolIndex, 0, index);
		}
		public String[] getGroups() {
			return groups;
		}

		public int getReturnedVal() {
			return returnedVal;
		}
	}
