package eg.edu.alexu.csd.oop.sql;

import java.io.File;
import java.sql.SQLException;
import eg.edu.alexu.csd.oop.xml.XmlReader;
import eg.edu.alexu.csd.oop.xml.XmlWriter;

public class InsertParser extends MyParser{
	
	private String curDb;
	
	public InsertParser(String db) {
		curDb = db;
	}
	
	@Override
	public Object parse(String query) throws SQLException {
		boolean col=false;
		String reg = "\\s*INSERT\\s+INTO\\s+(\\w+)(\\s*\\(.+\\)\\s*|\\s+)VALUES\\s*(\\(.+\\))\\s*";
		if (regexChecker(reg,query,query.length())){
			String[] Gs = getGroups();
			String table = Gs[0];
			String cols = Gs[1];
			String vals = Gs[2];
			// check columns
			if (cols.trim().length()>0){
				cols = cols.replaceAll("^(\\s*)|(\\s*)$","").replaceAll("^(\\()|(\\))$","").
						replaceAll("^(\\s*)|(\\s*)$","").replaceAll("\\s*,\\s*",",");
				if (regexChecker("\\w+(,\\w+)*",cols,cols.length())){
					col=true;
				}else throw new SQLException();
			}
			// check values
			vals = vals.replaceAll("^(\\()|(\\))$","").
					replaceAll("^(\\s*)|(\\s*)$","").replaceAll("\\s*,\\s*",",");
			if (!regexChecker("[\'\"]?\\w+[\'\"]?(,[\'\"]?\\w+[\'\"]?)*",vals,vals.length())) throw new SQLException();
			// check table 
			XmlReader xmlr=null;
			try{
				xmlr = new XmlReader(curDb+File.separator+table.toLowerCase()+".xml");
			}catch(RuntimeException ex){
				ex.printStackTrace();
			}
			int colen = xmlr.getAtrr().split("\\,").length;
			String[][]v = new String[1][colen];
			String[] values = vals.split("\\,");
			for (int i=0;i<values.length;i++){
				if (values[i].startsWith("\'")&&values[i].endsWith("\'")||values[i].startsWith("\"")&&values[i].endsWith("\"") ){
					values[i]=values[i].substring(1,values[i].length()-1);
				}
			}
			if (col){
				int[]indices = selectBeforeOperator(xmlr.getAtrr(),colen,cols);
				for (int i=0;i<indices.length;i++){
					v[0][indices[i]]=values[i];
				}
			}else{
				v[0]=values;
			}
			
			String[][] result = new String [xmlr.getEntries().length+1][];
	        System.arraycopy(xmlr.getEntries() , 0, result, 0, xmlr.getEntries().length);
	        System.arraycopy(v, 0, result, xmlr.getEntries().length, v.length);
	        new XmlWriter(new File(curDb+File.separator+table.toLowerCase()+".xml"),result,xmlr.getAtrr(),table);
	        
	        return 1;
	        
		}else throw new SQLException();
	}
}
