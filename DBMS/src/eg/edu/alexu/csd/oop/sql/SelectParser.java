package eg.edu.alexu.csd.oop.sql;
import java.io.File;
import java.sql.SQLException;
import eg.edu.alexu.csd.oop.xml.XmlReader;
public class SelectParser extends MyParser{
	private String query , dataBaseName , attribute,fileName,beforePart;
	private String[][] tableArray  ;
	private Object[][] updatedTable ;
	private int [] selectedColIndex ,selected ;
	private  XmlReader read;

	public SelectParser(String dataBase) {
		this.dataBaseName = dataBase;
	}

	@Override
	public Object parse(String query) throws SQLException {
		this.query = query;
		String check1 = "\\s*SELECT\\s+(\\w+[\\s*\\,\\s*\\w+]*?|\\*)\\s+FROM\\s+(\\w+)\\s*";
		String check2 = "\\s*SELECT\\s+(\\w+[\\s*\\,\\s*\\w+]*?|\\*)\\s+FROM\\s+(\\w+)\\s+WHERE\\s+(\\w+)\\s*([<,>,=])\\s*(.*)\\s*";
		
		if(this.regexChecker(check1 , this.query,this.query.length()))		
		{
			String []group = this.getGroups();
			String tableName =  group[1];
			this.beforePart = group[0];
			this.intialize(tableName);
			String[] attr =  this.attribute.split(",");
		    this.updatedTable = new Object[this.tableArray.length][this.selectedColIndex.length];
		    for(int i = 0 ; i < this.tableArray.length ; i++)
			{
				for(int j = 0 ; j < this.selectedColIndex.length ; j++)
				{
					String []type = attr[selectedColIndex[j]].split(";");
					if(type[1].equalsIgnoreCase("int"))
					 updatedTable[i][j] = Integer.parseInt(this.tableArray[i][selectedColIndex[j]]);
					else
						updatedTable[i][j] = this.tableArray[i][selectedColIndex[j]];	
				}
			}
		}
		 /////////////////////////////////////////////////////////////////
		else if(this.regexChecker(check2 , this.query,this.query.length()))  		 
		{	
			String []group = this.getGroups();
			String operator = group[3];
			String condition = group[2];
			String tableName =group[1];
			this.beforePart = group[0];
			String operandValue = group[4];
			intialize(tableName); 
			this.selected = this.selectAfterOperator(this.attribute, condition, this.tableArray, operator, operandValue);
		       if(this.getReturnedVal() == 0)
				 this.updatedTable = new Object[0][0];
			else
				this.updatedTable = new Object[this.getReturnedVal()][this.selectedColIndex.length];
			int index = 0 ;
			 String[] attr =  this.attribute.split(",");
			for(int i = 0 ; i < this.tableArray.length && (this.getReturnedVal() != 0); i++)
			{
				if(this.selected[i] == 1){
					for(int j = 0 ; j < this.selectedColIndex.length ; j++)
					{
						 String []type = attr[selectedColIndex[j]].split(";");
					    if(type[1].equalsIgnoreCase("int"))
						  updatedTable[index][j] = Integer.parseInt(this.tableArray[i][selectedColIndex[j]]);
					    else
					    	updatedTable[index][j] = this.tableArray[i][selectedColIndex[j]];
					}
					index++;
				}
			}
		}
		else{
			throw  new SQLException(this.query + "inside");
		}
		return this.updatedTable;
	}
	
	private void intialize(String tableName) throws SQLException
	{
		this.fileName = this.dataBaseName+File.separatorChar+tableName.toLowerCase()+".xml";
	    try {
			this.read = new XmlReader(fileName);
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	  this.tableArray = this.read.getEntries();
	  this.attribute = this.read.getAtrr();
	   this.selectedColIndex = this.selectBeforeOperator(this.attribute,  this.tableArray[0].length, this.beforePart);
	}	
}