package eg.edu.alexu.csd.oop.sql;

import java.io.File;
import java.sql.SQLException;

import eg.edu.alexu.csd.oop.xml.XmlReader;
import eg.edu.alexu.csd.oop.xml.XmlWriter;

public class UpdateParser extends MyParser{
	private String curDb;
	public UpdateParser (String db){
		curDb=db;
	}
	@Override
	public Object parse(String query) throws SQLException {
		boolean cond=false;
		String reg = "\\s*UPDATE\\s+(\\w+)\\s+SET\\s+(\\w+\\s*=\\s*\'?\\w+\'?(\\s*,\\s*\\w+\\s*=\\s*\'?\\w+\'?)*)(\\s+WHERE\\s+(\\w+\\s*[<=>]\\s*\'?\\w+\'?))?\\s*";
		if (regexChecker(reg,query,query.length())){
			String[]Gs = getGroups();
			String table = Gs[0];
			String cols_vals = Gs[1].replaceAll("\\s*\\,\\s*","\\,").replaceAll("\\s*=\\s*","=").replaceAll("\'(\\w+)\'","$1");
			String condition="";
			condition=query.replaceAll(reg,"$5");
			if (condition.length()>0)cond=true;
			else condition = condition.replaceAll("\\s*(<|=|>)\\s*","$1").replaceAll("\'(\\w+)\'","$1");
			XmlReader xmlr=null;
			try{
				xmlr = new XmlReader(curDb+File.separator+table.toLowerCase()+".xml");
			}catch(RuntimeException ex){
				throw new SQLException("line 31 "+query);
			}
			String[] s = cols_vals.split("\\,");
			String[] vals=new String[s.length];
			String column="";
			for (int i=0;i<s.length;i++){
				String[]p=s[i].split("=");
				vals[i]=p[1];
				column += p[0]+",";
			}column=column.substring(0,column.length()-1);
			String[][]entries = xmlr.getEntries();
			if (entries.length==0)return 0;
			int[]columns=selectBeforeOperator(xmlr.getAtrr(),entries[0].length,column);
			int[]rows=null;
			if (cond){
				String o1 = condition.replaceAll("(.+)([<=>])(.+)","$1");
				String o2 = condition.replaceAll("(.+)([<=>])(.+)","$3");
				String o = condition.replaceAll("(.+)([<=>])(.+)","$2");
				rows=selectAfterOperator(xmlr.getAtrr(),o1,entries,o,o2);	
			}
			int counter=0;
			for (int i=0;i<entries.length;i++){
				if (cond&&rows[i]==1||!cond){
					counter++;
					for (int j=0;j<columns.length;j++){
						entries[i][columns[j]]=vals[j];
					}
				}
			}
	        new XmlWriter(new File(curDb+File.separator+table.toLowerCase()+".xml"),entries,xmlr.getAtrr(),table);
	        return counter;
		}
		else throw new SQLException("line 61 "+query);
	}
}
