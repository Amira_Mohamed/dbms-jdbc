package eg.edu.alexu.csd.oop.db.interpreter;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SelectInterpreter implements Interpreter{

	@Override
	public LinkedList<String> interpret(String syntax) {
		LinkedList<String> list = new LinkedList<String>();
		String whereRegex = "FROM\\s+(\\w+)|WHERE\\s+(\\w+)\\s+(\\W)\\s+(\\'?\\w+\\'?)\\s*";
		Pattern pattern = Pattern.compile(whereRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(syntax);
		matcher.find();
		list.add(matcher.group(1));
		if (matcher.find()) {
			list.add("foundWhere");
			list.add(matcher.group(2));
			list.add(matcher.group(3));
			list.add(matcher.group(4));

		} else {
			list.add("notfoundWhere");
		}
		String columnRegex = "SELECT\\s+(\\w+)|,\\s*(\\w+)";
		pattern = Pattern.compile(columnRegex, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(syntax);
		if (matcher.find()) {
			list.add("specific");
			list.add(matcher.group(1));
			while (matcher.find()) {
				list.add(matcher.group(2));
			}
		} else {
			list.add("all");
		}
		return list;
	}

}
