package eg.edu.alexu.csd.oop.db.jdbc;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;

public class ResultSetMetaData implements java.sql.ResultSetMetaData{
	private LinkedList<String> columnName;
	private LinkedList<String> columnType;
	private String tableName = null ;
	
	public ResultSetMetaData(LinkedList<String> columnName, LinkedList<String> columnType,String tableName) {
		this.columnName = columnName;
		this.columnType = columnType;
		this.tableName = tableName ;
 	}
	@Override
	public int getColumnCount() throws SQLException {
		
		return this.columnName.size();
	
	}
	@Override
	public int getColumnType(int column) throws SQLException {
		if (column < 1 || column > columnType.size() || columnType == null) {
			throw new SQLException();
		}
		String type = columnType.get(column - 1);
		if (type.equalsIgnoreCase("varchar")) {
			return Types.VARCHAR;
		} else if (type.equalsIgnoreCase("int")) {
			return Types.INTEGER;
		}
		throw new SQLException();
	}
	@Override
	public String getTableName(int size) throws SQLException {
		if(size >= 1 && size <= columnName.size()){
		    return this.tableName ;
	    }else{
	    	throw new SQLException();
	    }
	}
	@Override
	public String getColumnLabel(int column) throws SQLException {
		try{
			return this.columnName.get(column - 1);
		} catch(Exception exception) {
			throw new SQLException();
		}
	}
	@Override
	public String getColumnName(int column) throws SQLException {
		return this.getColumnLabel(column);
	}

	
	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public String getCatalogName(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public String getColumnClassName(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}



	@Override
	public int getColumnDisplaySize(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public String getColumnTypeName(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int getPrecision(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int getScale(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public String getSchemaName(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}


	@Override
	public boolean isAutoIncrement(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isCaseSensitive(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isCurrency(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isDefinitelyWritable(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int isNullable(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isReadOnly(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isSearchable(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isSigned(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isWritable(int arg0) throws SQLException {
		throw new java.lang.UnsupportedOperationException();
	}

}