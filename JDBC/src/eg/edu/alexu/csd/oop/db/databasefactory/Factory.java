package eg.edu.alexu.csd.oop.db.databasefactory;

/**
 * The Class Factory.
 */
public class Factory {

	/**
	 * New instance.
	 *
	 * @param ClassName the class name
	 * @return the abstract directory
	 */
	public AbstractDirectory newInstance(String ClassName){
		
		if(ClassName.equalsIgnoreCase("table")){
			return new Table() ;
		}else{
			return new DatabaseCreation() ;
		}
	}
}
