package eg.edu.alexu.csd.oop.db;

import java.util.LinkedList;

public interface DataInfo {

	/**
	 * Gets the column type.
	 *
	 * @return the column type
	 */
	public LinkedList<String> getColumnType();

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public LinkedList<String> getColumnName();

	public void setSelectedTable(LinkedList<String> selectedTable);

	public void setDataBasePath(String dataBasePath);

}