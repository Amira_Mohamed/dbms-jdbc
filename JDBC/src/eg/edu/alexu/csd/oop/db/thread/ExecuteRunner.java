package eg.edu.alexu.csd.oop.db.thread;

import java.sql.SQLException;

import eg.edu.alexu.csd.oop.db.jdbc.Statement;

public class ExecuteRunner implements Runnable {
	private String sql = null;
	private Statement statement = null;
	private int start = 0;
	private int end = 0;
	private int time = 0;

	public ExecuteRunner(Statement statement) {
		this.statement = statement;
	}

	public int getExecutionTime() {
		return this.time;
	}

	public void setSQL(String sql) {
		this.sql = sql;
	}

	@Override
	public void run() {
		this.start = (int) System.currentTimeMillis();
		try {
			this.statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.end = (int) System.currentTimeMillis();
		this.time = end - start;
	}
}
