package eg.edu.alexu.csd.oop.db.interpreter;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateInterpreter implements Interpreter{

	@Override
	public LinkedList<String> interpret(String syntax) throws SQLException {
		LinkedList<String> list = new LinkedList<String>();
		int start = 0;
		int end = syntax.length();
		String whereRegex = "UPDATE\\s+(\\w+)|WHERE\\s+(\\w+)\\s*(\\W)\\s*(\\'?\\w+\\'?)\\s*";
		Pattern pattern = Pattern.compile(whereRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(syntax);
		matcher.find();
		list.add(matcher.group(1));
		if (matcher.find()) {
			list.add("foundWhere");
			list.add(matcher.group(2));
			list.add(matcher.group(3));
			list.add(matcher.group(4));
			String regex = "SET|WHERE";
			Pattern compiledPattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcherPattern = compiledPattern.matcher(syntax);
			matcherPattern.find();
			start = matcherPattern.end();
			matcherPattern.find();
			end = matcherPattern.start();
		} else {
			String regex = "SET";
			Pattern compiledPattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcherPattern = compiledPattern.matcher(syntax);
			matcherPattern.find();
			start = matcherPattern.end();
			list.add("notfoundWhere");
		}
		// set values
		String setRegex = "(\\w+\\s*\\W\\s*\\'?\\w+\\'?)";
		Pattern setPattern = Pattern.compile(setRegex, Pattern.CASE_INSENSITIVE);
		Matcher setMatcher = setPattern.matcher(syntax);
		setMatcher = setMatcher.region(start, end);
		list.add("set");
		while (setMatcher.find()) {
			list.add(setMatcher.group(1));
		}

		return list;
	}
  
}
