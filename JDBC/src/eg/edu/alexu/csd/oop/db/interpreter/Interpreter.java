package eg.edu.alexu.csd.oop.db.interpreter;

import java.sql.SQLException;
import java.util.LinkedList;

public interface Interpreter {
  LinkedList<String> interpret(String syntax) throws SQLException;
}
