package eg.edu.alexu.csd.oop.db.jdbc;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class Driver implements java.sql.Driver {
	private DriverPropertyInfo[] propertyInfo;
	private Properties properties ;

	public Driver(){
		 propertyInfo = new DriverPropertyInfo[1];
		 properties = new Properties();
		 
	}
	public String getPath(){		
		return new File (this.properties.get("path").toString()).getAbsolutePath();
	}
	@Override
	public boolean acceptsURL(String arg0) throws SQLException {
		return true;
	}

	@Override
	public Connection connect(String string, Properties properties) throws SQLException {
		this.properties = properties;
		return new eg.edu.alexu.csd.oop.db.jdbc.Connection(this);
	}
	@Override
	public DriverPropertyInfo[] getPropertyInfo(String path, Properties properties)
			throws SQLException {
		DriverPropertyInfo property = new DriverPropertyInfo("path", path);
		propertyInfo[0] = property;
		return propertyInfo;
	}

	@Override
	public int getMajorVersion() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getMinorVersion() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean jdbcCompliant() {
		throw new UnsupportedOperationException();
	}

}