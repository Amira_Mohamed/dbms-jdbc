package eg.edu.alexu.csd.oop.db.databasefactory;

import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import eg.edu.alexu.csd.oop.db.jdbc.Connection;
import eg.edu.alexu.csd.oop.db.jdbc.Driver;
import eg.edu.alexu.csd.oop.db.jdbc.ResultSet;
import eg.edu.alexu.csd.oop.db.jdbc.ResultSetMetaData;
import eg.edu.alexu.csd.oop.db.jdbc.Statement;
import eg.edu.alexu.csd.oop.db.thread.ExecuteQueryRunner;
import eg.edu.alexu.csd.oop.db.thread.ExecuteRunner;
import eg.edu.alexu.csd.oop.db.thread.ExecuteUpdateRunner;

public class AppLauncher {
	
	public static void main(String[] args) throws SQLException {
		try {
			String sql;
			String splitedQuery;
			Driver driver = new Driver();
			Properties info = new Properties();
			info.put("path", System.getProperty("user.dir"));
			Connection connection = (Connection) driver.connect("jdbc:xmldb://localhost", info);
			Scanner scanner = new Scanner(System.in);
			Statement statement = (Statement) connection.createStatement();
			System.out.println("Connection established");
			System.out.println("Define Maximum Execution Time, Please !!");
			statement.setQueryTimeout(scanner.nextInt());
			scanner.nextLine() ;	
			ExecuteRunner executeRunner = new ExecuteRunner(statement);
			ExecuteQueryRunner executeQueryRunner = new ExecuteQueryRunner(statement);
			ExecuteUpdateRunner executeUpdateRunner = new ExecuteUpdateRunner(statement);
			Thread excuteThread;
			Thread excuteQueryThread;
			Thread excuteUpdateThread;
			
			System.out.println("Sql Query >>");
			while (scanner.hasNext()) {
				try {
					sql = scanner.nextLine();
					splitedQuery = sql.split("\\s+")[0].toLowerCase();
					if (splitedQuery.equals("drop") || splitedQuery.equals("create")) {
						excuteThread = new Thread(executeRunner);
						executeRunner.setSQL(sql);
						excuteThread.start();
						Thread.sleep(statement.getQueryTimeout());
						if (excuteThread.isAlive() && statement.getQueryTimeout() != 0) {
							excuteThread.join();
							System.out.println("OOPS .. Too Much Time" + executeRunner.getExecutionTime() + " > " + statement.getQueryTimeout());
						} else {
							excuteThread.join();
							System.out.println("Query executed at (" + executeRunner.getExecutionTime() + " ms)");
						}
					} else if ("select".equalsIgnoreCase(sql.split(" ")[0])) {
						
						excuteQueryThread = new Thread(executeQueryRunner);
						executeQueryRunner.setSQL(sql);
						excuteQueryThread.start();
						Thread.sleep(statement.getQueryTimeout());
						if (excuteQueryThread.isAlive() && statement.getQueryTimeout() != 0) {
							excuteQueryThread.join();
								System.out.println("OOPS .. Too Much Time" + executeQueryRunner.getExecutionTime() + " > " + statement.getQueryTimeout());
						} else {
							excuteQueryThread.join();
							System.out.println("mAyar1");
							ResultSet resultSet = (ResultSet) executeQueryRunner.getResultSet();
							System.out.println("mAyar2");
							ResultSetMetaData metaData = (ResultSetMetaData) resultSet.getMetaData();
							printSelectedTable(resultSet, metaData);
							System.out.println("Query executed at (" + executeQueryRunner.getExecutionTime() + " ms)");
						}
					} else {
						excuteUpdateThread = new Thread(executeUpdateRunner);
						executeUpdateRunner.setSQL(sql);
						excuteUpdateThread.start();
						Thread.sleep(statement.getQueryTimeout());
						if (excuteUpdateThread.isAlive() && statement.getQueryTimeout() != 0) {
							excuteUpdateThread.join();
								System.out.println("OOPS .. Too Much Time"+ executeUpdateRunner.getExecutionTime() + " > "+ statement.getQueryTimeout());
						} else {
							excuteUpdateThread.join();
							System.out.println("#Updated Rows = " + executeUpdateRunner.getUpdatedRowsCount());
							System.out.println("Query executed at (" + executeUpdateRunner.getExecutionTime() + " ms)");
						}
					}

				} catch (SQLException exception) {
					System.out.println("Statement Cannot be executed.");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Sql Query >>");
			}
			scanner.close();
		} catch (SQLException e) {
			System.out.println("Check your connection again.");
		}
	}

	public static void printSelectedTable(ResultSet set, ResultSetMetaData data)
			throws SQLException {
		System.out.println("\nTable Name : " + data.getTableName(1));
		int columnCount = 0, index = 1, counter = 1;

		// print columns' names
		if (set.absolute(counter)) {
			columnCount = data.getColumnCount();
			while (index <= columnCount) {
				System.out.printf("|%-15s|", data.getColumnName(index));
				index++;
			}
			System.out.println("\n");
		}

		// print columns' data
		while (set.absolute(counter)) {
			for (int i = 1; i <= columnCount; i++) {
				System.out.printf("|%-15s|", set.getObject(i));
			}
			System.out.println();
			counter++;
		}
	}
}
