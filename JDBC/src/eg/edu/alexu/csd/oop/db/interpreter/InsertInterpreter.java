package eg.edu.alexu.csd.oop.db.interpreter;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InsertInterpreter implements Interpreter{

	@Override
	public LinkedList<String> interpret(String syntax) throws SQLException {
		String regex = "INTO\\s+(\\w+)\\s*";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(syntax);
		matcher.find();
		LinkedList<String> mainList = new LinkedList<String>();
		mainList.add(matcher.group(1));
		LinkedList<String> columnList = new LinkedList<String>();
		LinkedList<String> valueList = new LinkedList<String>();
		String value = "VALUES";
		Pattern patternValue = Pattern.compile(value, Pattern.CASE_INSENSITIVE);
		Matcher matcherValue = patternValue.matcher(syntax);
		matcherValue.find();
		int start = matcherValue.end();
		int end = matcherValue.start();
		String regexValue = "\\(?,?\\s*(\\'?\\w+\\'?)";
		Pattern valuePattern = Pattern.compile(regexValue, Pattern.CASE_INSENSITIVE);
		Matcher valueMatcher = valuePattern.matcher(syntax);
		valueMatcher.find(start);
		valueList.add(valueMatcher.group(1));
		while (valueMatcher.find()) {
			valueList.add(valueMatcher.group(1));
		}

		String intoRegex = "INTO\\s+\\w+\\s*";
		Pattern patternColumn = Pattern.compile(intoRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcherColumn = patternColumn.matcher(syntax);
		matcherColumn.find();
		int columnStart = matcherColumn.end();
		String columnRegex = ",?\\s*(\\w+)";
		Pattern columnPattern = Pattern.compile(columnRegex, Pattern.CASE_INSENSITIVE);
		Matcher columnMatcher = columnPattern.matcher(syntax);
		columnMatcher = columnMatcher.region(columnStart, end);
		while (columnMatcher.find()) {
			columnList.add(columnMatcher.group(1));
		}
		if (columnList.isEmpty()) {
			mainList.add("valuesOnly");
			mainList.addAll(valueList);
		} else {
			mainList.add("columnValue");
			if (columnList.size() != valueList.size())
				throw new SQLException();
			for (int i = 0; i < columnList.size(); i++) {
				mainList.add(columnList.get(i) + "=" + valueList.get(i));
			}
		}
		return mainList;
	}

}
