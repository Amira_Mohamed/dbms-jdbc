/*
 * 
 */
package eg.edu.alexu.csd.oop.db.databasefactory;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import eg.edu.alexu.csd.oop.db.data.InfoFile;
import eg.edu.alexu.csd.oop.db.xml.DTD;
import eg.edu.alexu.csd.oop.db.xml.XmlParser;

/**
 * The Class Table.
 */
public class Table extends AbstractDirectory  {

	/**
	 * Check existence.
	 *
	 * @param tableName the table name
	 * @param DatabasePath the data base path
	 * @return true, if successful
	 */
	public boolean checkExistence(String tableName, String DatabasePath){
		tableName = tableName.toLowerCase();
        try{
		File database = new File(DatabasePath);
		String[] tables = database.list();
		for (String table : tables) {
			if (table.toLowerCase().equals(tableName.toLowerCase() + ".xml")) {
				return true;
			}
		}
        }catch(Exception e )
        {
        	return false ;
        }
		return false;
	}

	@Override
	public boolean create(LinkedList<String> tableQuery, String databasePath)  {
		if (checkExistence(tableQuery.get(2), databasePath)) {
			return false ;
			
		}
		if (!(new File(databasePath).exists())) {
			throw new RuntimeException();
		}
		File table = new File(databasePath + File.separator + tableQuery.get(2).toLowerCase() + ".xml");
		InfoFile infoFile = new InfoFile();
		infoFile.saveTableInfo(tableQuery, databasePath);
		try {
			DTD dtd = new DTD();
			dtd.SaveDtdFile(tableQuery, databasePath , infoFile.getAttributes());
			return table.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean drop(LinkedList<String> tableQuery, String DatabasePath)  {
		if (checkExistence(tableQuery.get(2), DatabasePath)) {
			File table = new File(DatabasePath.toString() + File.separator + tableQuery.get(2).toLowerCase() + ".xml");
			File dtd = new File(DatabasePath.toString() + File.separator + tableQuery.get(2).toLowerCase() + ".dtd");
			return (table.delete() && dtd.delete());
		}
		return false ; 
	}

	/**
	 * Update.
	 *
	 * @param tableQuery the table query
	 * @param path the path
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int update(LinkedList<String> tableQuery, String path) throws SQLException {
		XmlParser xmlTable = new XmlParser();
		Map<String, LinkedList<Object>> dataTable = new LinkedHashMap<String, LinkedList<Object>>();
		dataTable = xmlTable.loadXml(tableQuery.get(0).toLowerCase(), path);
		if (dataTable.isEmpty()) {
			return 0;
		}
		Map<Integer, Boolean> validRow = new LinkedHashMap<Integer, Boolean>();
		boolean whereWord = false;
		if (tableQuery.get(1).equals("whereWord")) {
			whereWord = true;
			String column = tableQuery.get(2);
			String operator = tableQuery.get(3);
			String value = tableQuery.get(4);
			LinkedList<Object> temp = new LinkedList<Object>();
			temp = dataTable.get(column);
			if (temp == null)
				return 0;
			for (int data = 0; data < temp.size(); data++) {
				if (operator.equals(">") && Integer.parseInt(String.valueOf(temp.get(data))) > Integer.parseInt(value)) {
					validRow.put(data, true);
					// parse integer
				} else if (operator.equals("<")
						&& Integer.parseInt(String.valueOf(temp.get(data))) < Integer.parseInt(value)) {
					validRow.put(data, true);

					// ignore case
				} else if (operator.equals("=") && temp.get(data).toString().toLowerCase().equals((value.toLowerCase()))) {
					validRow.put(data, true);
				}
			}
		}
		Map<String, String> setColumn = new LinkedHashMap<String, String>();
		for (int query = tableQuery.indexOf("set") + 1; query < tableQuery.size(); query++) {
			String[] string = tableQuery.get(query).split("=|<|>");
			setColumn.put(string[0], string[1]);
		}
		int size = 0;
		if (!whereWord) {
			for (String column : setColumn.keySet()) {
				LinkedList<Object> temp = new LinkedList<Object>();
				size = dataTable.get(column).size();
				for (int iterator = 0; iterator < size; iterator++) {
					temp.add(setColumn.get(column));
				}
				dataTable.put(column, temp);
			}

		} else {
			for (String column : setColumn.keySet()) {
				LinkedList<Object> temp = new LinkedList<Object>();
				temp = dataTable.get(column);
				size = validRow.size();
				for (Integer iterator : validRow.keySet()) {
					temp.remove(iterator.intValue());
					temp.add(iterator.intValue(), setColumn.get(column));
				}

				dataTable.put(column, temp);
			}
		}

		xmlTable.SaveXml(dataTable, tableQuery.get(0).toLowerCase(), path);
		return size;
	}

	/**
	 * Insert.
	 *
	 * @param tableQuery the table query
	 * @param databasepath the data basepath
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int insert(LinkedList<String> tableQuery, String databasepath) throws SQLException {
		XmlParser xmlTable = new XmlParser();
		Map<String, LinkedList<Object>> dataFile = new LinkedHashMap<String, LinkedList<Object>>();

		dataFile = xmlTable.loadXml(tableQuery.get(0).toLowerCase(), databasepath);

		if (dataFile.isEmpty()) {
			InfoFile infoFile = new InfoFile();
			LinkedList<String> columnInfo = new LinkedList<String>();
			columnInfo = infoFile.LoadTableInfo(databasepath + File.separator + tableQuery.get(0).toLowerCase() + ".xml");
			for (int i = 0; i < columnInfo.size(); i++) {
				dataFile.put(columnInfo.get(i).split(" ")[0], new LinkedList<Object>());
			}
		}
		if (tableQuery.get(1).equalsIgnoreCase("valuesOnly")) {
			int it = 2;
			for (String column : dataFile.keySet()) {
				LinkedList<Object> list = new LinkedList<Object>();
				list = dataFile.get(column);
				if (it < tableQuery.size()) {
					list.add(tableQuery.get(it++));
				} else {
					list.add("null");
				}
				dataFile.put(column, list);
			}
		} else {
			Map<String, String> existColumn = new LinkedHashMap<String, String>();
			for (int i = 2; i < tableQuery.size(); i++) {
				String[] string = tableQuery.get(i).split("=");
				existColumn.put(string[0], string[1]);
			}
			for (String column : dataFile.keySet()) {
				LinkedList<Object> list = new LinkedList<Object>();
				list = dataFile.get(column);
				if (existColumn.containsKey(column)) {
					list.add(existColumn.get(column));
				} else {
					list.add("null");
				}
				dataFile.put(column, list);
			}
		}
		xmlTable.SaveXml(dataFile, tableQuery.get(0).toLowerCase(), databasepath);
		return 1;
	}

	/**
	 * Select.
	 *
	 * @param tableQuery the table query
	 * @param databasepath the data basepath
	 * @return the object[][]
	 * @throws SQLException the SQL exception
	 */
	public Object[][] select(LinkedList<String> tableQuery, String databasepath) throws SQLException {
		XmlParser xmlTable = new XmlParser();
		if (!checkExistence(tableQuery.get(0).toLowerCase(), databasepath)) {
			throw new SQLException();
		}
		Map<Integer, Boolean> validRow = new LinkedHashMap<Integer, Boolean>();
		String column = null;
		String value = null;
		String operator = null;
		Map<String, LinkedList<Object>> dataTable = new LinkedHashMap<String, LinkedList<Object>>();
		dataTable = xmlTable.loadXml(tableQuery.get(0).toLowerCase(), databasepath);
		if (dataTable.isEmpty()) {
			return new Object[0][0];

		}

		LinkedList<LinkedList<Object>> result = new LinkedList<LinkedList<Object>>();
		LinkedList<Object> list = new LinkedList<Object>();
		boolean whereWord = false;
		boolean foundColumn = false;
		if (tableQuery.get(1).equals("whereWord")) {
			whereWord = true;
			column = tableQuery.get(2);
			operator = tableQuery.get(3);
			value = tableQuery.get(4);

			list = dataTable.get(column);
			if (list == null)
				return new Object[0][0];
			for (int data = 0; data < list.size(); data++) {
				foundColumn = true;
				if (operator.equals(">") && Integer.parseInt(String.valueOf(list.get(data))) > (Integer.parseInt(value))) {
					validRow.put(data, true);
				} else if (operator.equals("<")
						&& Integer.parseInt(String.valueOf(list.get(data))) < (Integer.parseInt(value))) {
					validRow.put(data, true);
				} else if (operator.equals("=") && list.get(data).toString().equalsIgnoreCase(value)) {
					validRow.put(data, true);
				}
			}
		}
		if (tableQuery.contains("specific")) {
			for (int length = tableQuery.indexOf("specific") + 1; length < tableQuery.size(); length++) {
				list = dataTable.get(tableQuery.get(length));
				LinkedList<Object> specificList = new LinkedList<Object>();
				for (int j = 0; j < list.size(); j++) {
					if (whereWord && foundColumn) {
						if (validRow.containsKey(j)) {
							specificList.add(list.get(j));
						}
					} else {
						specificList.add(list.get(j));
					}
				}
				result.add(specificList);
			}
		} else {
			for (Map.Entry<String, LinkedList<Object>> entry : dataTable.entrySet()) {
				list = entry.getValue();
				LinkedList<Object> specificList = new LinkedList<Object>();
				for (int j = 0; j < list.size(); j++) {
					if (whereWord && foundColumn) {
						if (validRow.containsKey(j)) {
							specificList.add(list.get(j));
						}
					} else {
						specificList.add(list.get(j));
					}
				}
				result.add(specificList);
			}
		}
		try {
			int row = result.getFirst().size();
			int col = result.size();
			Object[][] answer = new Object[row][col];
			for (int j = 0; j < col; j++) {
				LinkedList<Object> t = new LinkedList<Object>();
				t = result.get(j);
				for (int i = 0; i < row; i++) {
					answer[i][j] = t.get(i);
				}
			}
			return answer;
		} catch (Exception e) {
			return new Object[0][0];
		}
	}

	/**
	 * Delete.
	 *
	 * @param query the query
	 * @param path the path
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int delete(LinkedList<String> query, String path) throws SQLException {
		XmlParser xmlTable = new XmlParser();
		Map<String, LinkedList<Object>> data = new LinkedHashMap<String, LinkedList<Object>>();
		data = xmlTable.loadXml(query.get(0).toLowerCase(), path);
		if (data.isEmpty()) {
			return 0;
		}
		Map<Integer, Boolean> validRow = new LinkedHashMap<Integer, Boolean>();
		Iterator<String> it = data.keySet().iterator();
		int length = data.get(it.next()).size();
		boolean whereWord = false;
		if (query.get(1).equals("whereWord")) {
			whereWord = true;
			String column = query.get(2);
			String operator = query.get(3);
			String value = query.get(4);
			LinkedList<Object> list = new LinkedList<Object>();
			list = data.get(column);
			if (list == null)
				return 0;
			for (int i = 0; i < list.size(); i++) {
				if (operator.equals(">") && Integer.parseInt(String.valueOf(list.get(i))) > Integer.parseInt(value)) {
					validRow.put(i, true);
				} else if (operator.equals("<")
						&& Integer.parseInt(String.valueOf(list.get(i))) < Integer.parseInt(value)) {
					validRow.put(i, true);
				} else if (operator.equals("=") && list.get(i).toString().equalsIgnoreCase((value))) {
					validRow.put(i, true);
				}
			}
		}

		if (!whereWord) {

			xmlTable.SaveXml(new LinkedHashMap<String, LinkedList<Object>>(), query.get(0).toLowerCase(), path);
			return length;
		} else {
			for (int i = 0; i < length; i++) {
				if (validRow.containsKey(i)) {
					for (String column : data.keySet()) {
						data.get(column).remove(i);
						data.get(column).add(i, "#");

					}
				}
			}

			for (String column : data.keySet()) {
				LinkedList<Object> list = new LinkedList<Object>();
				list = data.get(column);
				while (list.contains("#")) {
					list.remove("#");
				}
				data.put(column, list);
			}
			xmlTable.SaveXml(data, query.get(0).toLowerCase(), path);
		}
		return validRow.size();
	}

}
