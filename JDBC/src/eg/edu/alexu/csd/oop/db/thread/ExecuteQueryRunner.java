package eg.edu.alexu.csd.oop.db.thread;

import java.sql.SQLException;

import eg.edu.alexu.csd.oop.db.jdbc.ResultSet;
import eg.edu.alexu.csd.oop.db.jdbc.Statement;

public class ExecuteQueryRunner implements Runnable {
	private String sql = null;
	private Statement statement = null;
	private ResultSet resultSet;
	private int start = 0;
	private int end = 0;
	private int time = 0;

	public ExecuteQueryRunner(Statement statement) {
		this.statement = statement;
	}

	public int getExecutionTime() {
		return this.time;
	}

	public ResultSet getResultSet() {
		return this.resultSet;
	}

	public void setSQL(String sql) {
		this.sql = sql;
	}

	@Override
	public void run() {
		this.start = (int) System.currentTimeMillis();
		try {
			this.resultSet = (ResultSet) statement.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.end = (int) System.currentTimeMillis();
		this.time = end - start;
	}
}
